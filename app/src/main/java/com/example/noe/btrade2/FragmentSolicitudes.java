package com.example.noe.btrade2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import java.util.ArrayList;


public class FragmentSolicitudes extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<solicitudes>  listitems = new ArrayList<>();
    RecyclerView MyRecyclerView;
    String key[] ;
    String nombreUsuario;
    String nombreSolicitante;
    String[] n;
    String nombreLibro;
    String ImagenSolicitante;
    String estado;

    private static final String TAG = "Solicitudes";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;

    public static FragmentSolicitudes newInstance() {
        return new FragmentSolicitudes();
    }

    public FragmentSolicitudes() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listitems.clear();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("solicitud");
        mAuth = FirebaseAuth.getInstance();

        Picasso.with(this.getContext()).setIndicatorsEnabled(true);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener(){
            public void onDataChange(DataSnapshot dataSnapshot){

                //Verificacion de Libros que le pertenecen a un usuario
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    key = ds.getKey().toString().split("@");
                    nombreUsuario = key[2];
                    nombreSolicitante= key[1];
                    estado=ds.child("estado").getValue().toString();
                    if(nombreUsuario.equals(mAuth.getCurrentUser().getUid())&& estado.equals("pendiente")){
                        n=ds.child("solicitante").getValue().toString().split("@");
                        nombreLibro = ds.child("libro").getValue().toString().toUpperCase();
                        solicitudes s= new solicitudes(nombreUsuario,n[0],n[1],nombreLibro,estado);
                        s.setIdSolicitud(key[0]+"@"+key[1]+"@"+key[2]);
                        listitems.add(s);

                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "CANCELADO");
            }
        });

        initializeList();
        getActivity().setTitle("Solicitud");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

      View v= inflater.inflate(R.layout.fragment_solicitudes,container,false);
        mRecyclerView = (RecyclerView)v.findViewById(R.id.mreciclerview);

    // use this setting to improve performance if you know that changes
       // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        // specify an adapter (see also next example)
        mAdapter = new MyAdapterSolicitud(this.getContext(),listitems);
       /// mAdapter= new SongAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return v;
    }

    public class MyAdapterSolicitud extends RecyclerView.Adapter<MyviewHolder>{
        private ArrayList<solicitudes> lista;
        Context sContext;

        public MyAdapterSolicitud(Context context,ArrayList<solicitudes> lista) {
            this.lista = lista;
            sContext = context;
        }

        @Override
        public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_solicitud,parent,false);
                MyviewHolder holder= new MyviewHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(MyviewHolder holder, final int position) {

            Picasso.with(sContext).load(lista.get(position).getImagenSolicitante()).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(holder.imagen);
            holder.nombreSol.setText(lista.get(position).getNombreSolicitante());
            holder.libro.setText(lista.get(position).getLibro_Solicitado());
            holder.aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDatabase.child(listitems.get(position).getIdSolicitud()).child("estado").setValue("aceptado");
                    listitems.remove(position);
                    //lista.addAll(listitems);
                    notifyDataSetChanged();

                    Toast toast = Toast.makeText(sContext, "aceptado", Toast.LENGTH_SHORT);
                    toast.show();

                }
            });
            holder.rechazar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDatabase.child(listitems.get(position).getIdSolicitud()).child("estado").setValue("rechazado");
                    listitems.remove(position);
                    Toast toast = Toast.makeText(sContext, "Rachazado", Toast.LENGTH_SHORT);
                    toast.show();




                }
            });



        }

        @Override
        public int getItemCount() {
            return lista.size();
        }
    }


    public class MyviewHolder extends RecyclerView.ViewHolder{
        public ImageView imagen;
        public TextView nombreSol;
        public TextView libro;
        public Button aceptar;
        public Button rechazar;

        public MyviewHolder(View v) {
            super(v);

            imagen=(ImageView)v.findViewById(R.id.usuario_image);
            nombreSol=(TextView)v.findViewById(R.id.nombre);
            libro=(TextView)v.findViewById(R.id.libroSolicitante);
            aceptar=(Button)v.findViewById(R.id.aceptar);
            rechazar=(Button)v.findViewById(R.id.rechazar);
           /* aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });
            rechazar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDatabase.child(listitems.get())

                }
            });*/





        }
    }



    public void initializeList() {
        listitems.clear();
/*
        for(int i =0;i<3;i++){


            solicitudes item = new solicitudes();
            item.setNombreSolicitante(NombreSolicitante[i]);
            item.setImagenSolicitante("");
            item.setLibro_Solicitado(Libro[i]);
            listitems.add(item);

        }*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}


