package com.example.noe.btrade2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AuxActivity extends AppCompatActivity {


    private DatabaseReference mDatabase;
    private String mCurrentUserEmail;
    private User currentUser;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aux);

        Intent intent = getIntent() ;
        mCurrentUserEmail = intent.getStringExtra(LoginActivity.CURRENT_USER);

        //el formato pasado es nombredeusuario@...
        //args[0] contendra nombredeusuario
        String args[] = mCurrentUserEmail.split("@");
        String user = args[0];

        //se obtiene la base de datos especifica del usuario
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(user);


        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Se obtienen los datos almacenados solo una vez
                //users:
                //  usuario:
                //      email:  valor
                //      id:     valor
                //      name:   valor
                //      phone   valor

                User user = dataSnapshot.getValue(User.class);
                Log.d("NAME ", user.getName());
                Log.d("ID ", user.getId());
                Log.d("EMAIL ", user.getEmail());
                Log.d("PHONE ", user.getPhone());
                currentUser = user;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //Base de datos de libros
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("books");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override


            public void onDataChange(DataSnapshot dataSnapshot) {
                //Iteracion sobre todos los usuarios en users.
                // getChildren obtiene los campos de cada usuario: name, email, id, phone
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    //ds: hijos directos de la bd books

                    //Se obtinen cada campo y su valor:
                    //book{
                    //  author: v_author
                    //  genre: {g1, g2, g3
                    //  language: v_language
                    //  owner: {p1, p2, p3}  --> BORRAR
                    //  title: v_title
                    // }
                    //  ds = book
                    // ********** CAMBIAR ID ds ds --> ds.value
                    // ds.children = {author, genre, language, owner, title}
                    //Iteracion entre los campos de un libro
                    for(DataSnapshot d : ds.getChildren()) {
                        //d = cada campo que tiene ds
                        Log.d("DEBUGGGGG", d.getKey()+"  "+d.getValue());
                    }



                    //***Obtencion manual de los campos sin iteracion
                    //Log.d("ID : ", " "+ds.getKey());
                    //Log.d("AUTOR : ", " "+ds.child("author").getKey()+" "+ds.child("author").getValue());
                    //Log.d("TITULO : ", " "+ds.child("title").getKey()+" "+ds.child("title").getValue());
                    //Log.d("GENERO : ", " "+ds.child("genre").getKey()+" "+ds.child("genre").getValue());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
