package com.example.noe.btrade2;

public class GBItem {

    private String mName;
    private int mPicutureResId;

    public GBItem(String name, int upic) {
        mName = name;
        mPicutureResId = upic;
    }

    public int getPictureResId() {return mPicutureResId;}
    public void setPictureResId(int picId) {mPicutureResId = picId;}
    public String getName() {return mName;}
    public void setName(String name) {mName = name;}

    public String toString (){
        return mName+" "+mPicutureResId;
    }

}
