package com.example.noe.btrade2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class MyBooksActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mBookAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Book> mBooks;

    private String mCurrentUserId;
    private static final String TAG = "My Books Activity";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mis_libros);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("books");
        mAuth = FirebaseAuth.getInstance();

        mCurrentUserId = mAuth.getCurrentUser().getUid();
        mBooks = new ArrayList<Book>();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_books);


        mLayoutManager = new LinearLayoutManager(MyBooksActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.setHasFixedSize(true);
        mBookAdapter = new BookAdapter(MyBooksActivity.this, mBooks, 0);
        mRecyclerView.setAdapter(mBookAdapter);

        Picasso.with(getApplicationContext()).setIndicatorsEnabled(true);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener(){
            public void onDataChange(DataSnapshot dataSnapshot){
                String bookId[];
                String title, author, description, owner, imageSource, id;
                Book book;

                //Verificacion de Libros que le pertenecen a un usuario
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    bookId = ds.getKey().toString().split("@");
                    id = bookId[1];
                    if(id.equals( mCurrentUserId)  ) {
                        title = ds.child("title").getValue().toString();
                        author = ds.child("author").getValue().toString();
                        description = ds.child("description").getValue().toString();
                        owner = ds.child("owner").getValue().toString();
                        imageSource = ds.child("image").getValue().toString();
                        book = new Book(title, author, description, owner, imageSource);
                        book.setId(bookId[0]+"@"+bookId[1]);
                        mBooks.add(book);
                        mBookAdapter.notifyItemInserted(mBooks.size()-1);
                        Log.d(TAG, " " + bookId[0]+" "+imageSource);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "CANCELADO");
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
