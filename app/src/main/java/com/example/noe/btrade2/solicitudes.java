package com.example.noe.btrade2;



public class solicitudes {

   private String nombreSolicitante;
   private String imagenSolicitante;
   private String LibroSolicitado;
    private String usurio;
    private String estado;
    private String idSolicitud;
    public solicitudes(){}

    public solicitudes(String usuario, String nombreSolicitante, String imagenSolicitante, String libroSolicitado,String estado) {
        this.nombreSolicitante = nombreSolicitante;
        this.imagenSolicitante = imagenSolicitante;
        LibroSolicitado = libroSolicitado;
        this.usurio=usuario;
        this.estado=estado;
    }


    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getImagenSolicitante() {
        return imagenSolicitante;
    }

    public void setImagenSolicitante(String imagenSolicitante) {
        this.imagenSolicitante = imagenSolicitante;
    }

    public String getLibro_Solicitado() {
        return LibroSolicitado;
    }

    public void setLibro_Solicitado(String libro_Solicitado) {
        LibroSolicitado = libro_Solicitado;

    }

    public String getLibroSolicitado() {
        return LibroSolicitado;
    }

    public void setLibroSolicitado(String libroSolicitado) {
        LibroSolicitado = libroSolicitado;
    }

    public String getUsurio() {
        return usurio;
    }

    public void setUsurio(String usurio) {
        this.usurio = usurio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }
}
