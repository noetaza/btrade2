package com.example.noe.btrade2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Fragment_notifi extends Fragment{


    public static Fragment_notifi newInstance() {
        return new Fragment_notifi();
    }

    public Fragment_notifi() {
       }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


         return inflater.inflate(R.layout.fragment_notifi, container, false);
    }
}
