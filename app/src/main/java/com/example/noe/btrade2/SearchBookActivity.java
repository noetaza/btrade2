package com.example.noe.btrade2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchBookActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mBookAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Book> mBooks;

    private String mCurrentUserId;
    private static final String TAG = "Search Book Activity";
    private static final String EXTRA_BOOK_SEARCH=" com.example.noe.btrade2.search_book_activity.booksearch";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mis_libros);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Búsqueda...");

        Intent intent = getIntent();
        final String query = intent.getStringExtra(EXTRA_BOOK_SEARCH);

        mBooks = new ArrayList<Book>();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_books);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("books");
        mAuth = FirebaseAuth.getInstance();


        mLayoutManager = new LinearLayoutManager(SearchBookActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mBookAdapter = new BookAdapter(SearchBookActivity.this, mBooks, 1);
        mRecyclerView.setAdapter(mBookAdapter);

        Picasso.with(getApplicationContext()).setIndicatorsEnabled(true);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                String bookId[];
                String title, author, description, owner, imageSource, id;
                String query_element[] = {};
                Book book;

                if(query.contains("-")){query_element = query.split("-");}

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    title = ds.child("title").getValue().toString();
                    author = ds.child("author").getValue().toString();
                    description = ds.child("description").getValue().toString();
                    owner = ds.child("owner").getValue().toString();
                    imageSource = ds.child("image").getValue().toString();

                    if(query_element.length == 2 ){
                        if(title.toLowerCase().contains(query_element[0].toLowerCase() )
                                && author.toLowerCase().contains(query_element[1].toLowerCase())){
                            book = new Book(title, author, description, owner, imageSource);
                            book.setId(title+"@"+owner);
                            Log.d(TAG, book.getId());
                            mBooks.add(book);
                            mBookAdapter.notifyItemInserted(mBooks.size() - 1);
                        }
                    }
                    else {
                        if(title.toLowerCase().contains(query.toLowerCase())
                                || author.toLowerCase().contains(query.toLowerCase())){
                            book = new Book(title, author, description, owner, imageSource);
                            book.setId(title+"@"+owner);
                            mBooks.add(book);
                            mBookAdapter.notifyItemInserted(mBooks.size() - 1);
                        }
                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "CANCELADO");
            }
        });
    }

    public static Intent newIntent(Context packageContext, String search){
        Intent i = new Intent(packageContext, SearchBookActivity.class);
        i.putExtra(EXTRA_BOOK_SEARCH, search);
        return i;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
