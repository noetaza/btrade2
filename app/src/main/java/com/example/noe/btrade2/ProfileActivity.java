package com.example.noe.btrade2;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = "Profile Activity";
    ImageView imageProfile;
    ImageView imageBackground;

    private TextView mTextViewName;
    private TextView mTextViewPhone;
    private TextView mTextViewEmail;
    private TextView mTextViewGender;

    ArrayList<GBItem> items = new ArrayList<GBItem>();

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private User user;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageProfile = (ImageView)findViewById(R.id.c_profile);
        imageBackground = (ImageView)findViewById(R.id.c_bg);

        mTextViewName = (TextView) findViewById(R.id.name);
        mTextViewPhone = (TextView) findViewById(R.id.phonenumber);
        mTextViewEmail = (TextView) findViewById(R.id.emailaddress);
        mTextViewGender = (TextView) findViewById(R.id.gender);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_gen);
        mRecyclerView.setHasFixedSize(true);

        mTextViewName.setText("");mTextViewPhone.setText("");
        mTextViewEmail.setText("");mTextViewGender.setText("");

        mAuth = FirebaseAuth.getInstance();

        user = getIntent().getExtras().getParcelable("usuario");

        mTextViewName.setText(user.getName());
        mTextViewPhone.setText(user.getPhone());
        mTextViewEmail.setText(user.getEmail());
        mTextViewGender.setText(user.getGender());

        String imageUrl1 = user.getImgProfile();
        if (!imageUrl1.equals("default") || TextUtils.isEmpty(imageUrl1))
            Picasso.with(ProfileActivity.this).load(imageUrl1).into(imageProfile);
            Picasso.with(ProfileActivity.this).load(imageUrl1).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(imageProfile);


        String imageUrl2 = user.getImgBg();
        if (!imageUrl2.equals("default") || TextUtils.isEmpty(imageUrl2))
            Picasso.with(ProfileActivity.this).load(imageUrl2).into(imageBackground);


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    mStorage = FirebaseStorage.getInstance().getReference();
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users");

                    mDatabase.child(firebaseAuth.getCurrentUser().getUid()).child("favorite").addValueEventListener(new ValueEventListener() {
                        //Adapter Intereses
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot ds: dataSnapshot.getChildren()) {
                                //Log.d(TAG, ds.getKey().toString());
                                if(ds.getValue().toString().equals("true")){
                                    int resId = getResources().getIdentifier(ds.getKey().toString(), "drawable", getPackageName());
                                    String gb_name = getResources().getString(getApplicationContext().getResources().getIdentifier(ds.getKey().toString(), "string", getPackageName()));

                                    GBItem item = new GBItem(gb_name, resId);
                                    items.add(item);
                                }
                            }
                            //Log.d(TAG, ""+items.size());

                            mAdapter = new GBAdapter(ProfileActivity.this, items);
                            mRecyclerView.setAdapter(mAdapter);
                            mLayoutManager = new LinearLayoutManager(ProfileActivity.this);
                            mRecyclerView.setLayoutManager(mLayoutManager);

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            //...
                        }

                    });

                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }

            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            case R.id.editar:
                //metodoEdit()
                Intent intent = new Intent(this, EditProfileActivity.class);
                intent.putExtra("usuario", user);
                startActivity(intent);

                return true;

            case R.id.home:
                //metodoEdit()
                Intent intent1 = new Intent(this, Home.class);
                startActivity(intent1);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
