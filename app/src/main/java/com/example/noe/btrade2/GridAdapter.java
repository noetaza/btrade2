package com.example.noe.btrade2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<GridItem> arrayList;

    public GridAdapter(Context context, ArrayList<GridItem> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_grid, null);
        }

        TextView title = (TextView) convertView.findViewById(R.id.grid_title);
        title.setText(arrayList.get(position).getTitle());
        TextView author = (TextView) convertView.findViewById(R.id.grid_author);
        author.setText(arrayList.get(position).getAuthor());
        ImageView img_grid = (ImageView) convertView.findViewById(R.id.grid_img_book);
        Picasso.with(context).load(arrayList.get(position).getImageSource()).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(img_grid);

        return convertView;
    }

    public GridItem getCurrent(int position){ return arrayList.get(position); }

}
