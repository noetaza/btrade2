package com.example.noe.btrade2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class Fragment_recoment extends Fragment {
    //private String bookNames[] = {"asuitableboy", "infernaldevices", "lifeofpi"};
    private GridView gridView;
    private GridAdapter adapter;
    private ArrayList<GridItem> listitems = new ArrayList<>();
    String key[] ;

    String grid_title;
    String grid_author;
    String grid_owner;
    String grid_img;

    String userR;
    String currentUser;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;

    private static final String TAG = "Recomendados";

    public static Fragment_recoment newInstance() {
        return new Fragment_recoment();
    }

    public Fragment_recoment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listitems.clear();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("books");
        mAuth = FirebaseAuth.getInstance();

        Picasso.with(this.getContext()).setIndicatorsEnabled(true);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener(){
            public void onDataChange(DataSnapshot dataSnapshot){

                //Libros Recomendados
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    key = ds.getKey().toString().split("@");
                    currentUser = key[1];

                    if(!currentUser.equals(mAuth.getCurrentUser().getUid())){
                        grid_title = ds.child("title").getValue().toString();
                        grid_author = ds.child("author").getValue().toString();
                        grid_owner = ds.child("owner").getValue().toString();
                        grid_img = ds.child("image").getValue().toString();
                        GridItem book = new GridItem(grid_title, grid_author, grid_owner, grid_img);
                        listitems.add(book);

                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "CANCELADO");
            }
        });

        initializeList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recoment, container, false);

        gridView = (GridView) view.findViewById(R.id.grid_books);
        adapter = new GridAdapter(this.getContext(), listitems);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String currentBookId = adapter.getCurrent(position).getID();

                Intent intent = new Intent(getActivity(), ViewBook.class);
                intent.putExtra("ID", currentBookId);
                startActivity(intent);
            }
        });

        return view;
    }

    public void initializeList() {
        listitems.clear();

    }

}