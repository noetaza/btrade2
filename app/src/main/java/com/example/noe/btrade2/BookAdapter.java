package com.example.noe.btrade2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder>{
    private ArrayList<Book> mBookList = new ArrayList<Book>();
    private static ViewGroup mParent;
    private static Context sContext;

    private int mPreviousPosition = 0;
    private static final String TAG ="BOOK ADAPTER";
    private static int type;   //0: Descripcion para Mis Libros, 1: Descripcion para libros en busqueda

    public BookAdapter(Context context, ArrayList<Book> arrayList, int mtype) {
        sContext = context;
        mBookList = arrayList;
        type = mtype;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, parent, false);
        BookAdapter.ViewHolder viewHolder = new BookAdapter.ViewHolder(v);
        mParent = parent;
        return viewHolder;
    }

    public void onBindViewHolder(BookAdapter.ViewHolder holder, int position) {
        Book current = mBookList.get(position);
        holder.mId = current.getId();
        holder.mBookTitle.setText(current.getTitle());
        holder.mBookAuthor.setText(current.getAuthor());
        holder.mBookDescription.setText(current.getDescription());
        Picasso.with(sContext).load(current.getImageSource()).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(holder.mBookImage);


    }

    public int getItemCount() {return mBookList.size();}


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView mBookImage;
        TextView mBookTitle, mBookAuthor, mBookDescription;
        LinearLayout mLinearLayout;
        String mId;
        final String ID = "com.example.noe.btrade2.bookID";

        public ViewHolder (View v) {
            super(v);
            mLinearLayout = (LinearLayout) v.findViewById(R.id.item);
            mBookImage = (ImageView)v.findViewById(R.id.book_image);
            mBookTitle = (TextView) v.findViewById(R.id.book_title);
            mBookAuthor = (TextView) v.findViewById(R.id.book_author);
            mBookDescription = (TextView) v.findViewById(R.id.book_description);
            v.setOnClickListener(this);
        }

        public void onClick(View view){

            Context context = mParent.getContext();
            if(type == 0){
                Intent intent = BookDescription.newIntent(context, mId);
                context.startActivity(intent);
            }

            if(type == 1){
                Intent intent = new Intent(context, ViewBook.class);
                intent.putExtra("ID", mId);
                context.startActivity(intent);

            }
        }

    }

}
