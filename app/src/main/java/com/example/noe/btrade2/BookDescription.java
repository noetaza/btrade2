package com.example.noe.btrade2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BookDescription extends AppCompatActivity {
    public static String currentUser;
    private static final String EXTRA_BOOK_ID=" com.example.noe.btrade2.bookid";
    private ImageView mImageView;
    private TextView mTextViewTitle;
    private TextView mTextViewAuthor;
    private TextView mTextViewDescription;

    private DatabaseReference mDatabase;
    private final String TAG ="Book Description";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<GBItem> mGenres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_description_content);

        Intent intent = getIntent();
        String id = intent.getStringExtra(EXTRA_BOOK_ID);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String split[];
        split = id.split("@");

        getSupportActionBar().setTitle(split[0]);

        mGenres = new ArrayList<GBItem>();
        mRecyclerView = (RecyclerView) findViewById(R.id.book_genres);
        mRecyclerView.setHasFixedSize(true);

        mImageView = (ImageView) findViewById(R.id.book_image);
        mTextViewTitle = (TextView) findViewById(R.id.book_title);
        mTextViewAuthor = (TextView) findViewById(R.id.book_author);
        mTextViewDescription = (TextView) findViewById(R.id.book_description);
        Log.d(TAG, id);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("books").child(id);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String title, author, description, owner, imageSource, id;
                title = dataSnapshot.child("title").getValue().toString();
                author = dataSnapshot.child("author").getValue().toString();
                description = dataSnapshot.child("description").getValue().toString();
                owner = dataSnapshot.child("owner").getValue().toString();
                imageSource = dataSnapshot.child("image").getValue().toString();
                mTextViewTitle.setText(title);
                mTextViewAuthor.setText(author);
                mTextViewDescription.setText(description);
                Picasso.with(getApplicationContext()).load(imageSource).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(mImageView);

                for(DataSnapshot ds: dataSnapshot.child("genre").getChildren()){
                    int resId = getResources().getIdentifier(ds.getKey().toString(), "drawable", getPackageName());
                    String gb_name = getResources().getString(getApplicationContext().getResources().getIdentifier(ds.getKey().toString(), "string", getPackageName()));

                    GBItem item = new GBItem(gb_name, resId);
                    mGenres.add(item);
                }

                mAdapter = new GBAdapter(BookDescription.this, mGenres);
                mRecyclerView.setAdapter(mAdapter);
                mLayoutManager = new LinearLayoutManager(BookDescription.this);
                mRecyclerView.setLayoutManager(mLayoutManager);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static Intent newIntent(Context packageContext, String id){
        Intent i = new Intent(packageContext, BookDescription.class);
        i.putExtra(EXTRA_BOOK_ID, id);
        return i;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
