package com.example.noe.btrade2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.api.model.StringList;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class NewBook extends AppCompatActivity {

    private ImageView img;
    private Button guardar;
    private ImageButton addFoto;

    private EditText titulo;
    private EditText autor;
    private EditText descripcion;
    private Spinner  idioma;
    private int cont=0;

    private CheckBox opcionAventura;
    private CheckBox opcionCientificos;
    private CheckBox opcionComic;
    private CheckBox opcionDrama;
    private CheckBox opcionInstructivos;
    private CheckBox opcionNovela;
    private CheckBox opcionPoesia;
    private CheckBox opcionReferencia;
    private CheckBox opcionRomance;
    private CheckBox opcionSciFi;
    private CheckBox opcionTerror;

    private FirebaseAuth mFirebaseAuth;

    ArrayList<String> imagepaths;

    static final int REQUEST_IMAGE_CAPTURE=1;   //constante para captura de pantalla linea 116


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_new_book);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        //codigo para el retroceso o boton de atras en el toolBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        guardar= (Button)findViewById(R.id.button);
        idioma =(Spinner) findViewById(R.id.textIdioma);
        autor = (EditText) findViewById(R.id.textAutor);
        titulo=(EditText) findViewById(R.id.textTitulo);
        descripcion=(EditText) findViewById(R.id.textDescripcion);
        addFoto = (ImageButton) findViewById(R.id.AddFoto);

        //Checkbox
        opcionAventura      = (CheckBox) findViewById(R.id.opcion_aventura);
        opcionCientificos   = (CheckBox) findViewById(R.id.opcion_cientificos);
        opcionComic         = (CheckBox) findViewById(R.id.opcion_comic);
        opcionDrama         = (CheckBox) findViewById(R.id.opcion_drama);
        opcionInstructivos  = (CheckBox) findViewById(R.id.opcion_instructivos);
        opcionNovela        = (CheckBox) findViewById(R.id.opcion_novela);
        opcionPoesia        = (CheckBox) findViewById(R.id.opcion_poesia);
        opcionReferencia    = (CheckBox) findViewById(R.id.opcion_referencia);
        opcionRomance       = (CheckBox) findViewById(R.id.opcion_romance);
        opcionSciFi         = (CheckBox) findViewById(R.id.opcion_scifi);
        opcionTerror        = (CheckBox) findViewById(R.id.opcion_terror);

        mFirebaseAuth = FirebaseAuth.getInstance();

        //se definen algunos campos
        //final String cateGoria ="Novela";
        //final String Idioma="Ingles";

        //Array con paths de la imagenes de libros
        //imagepaths=new ArrayList<String>();

        img = (ImageView) findViewById(R.id.imageBook);

        idioma = (Spinner) findViewById(R.id.textIdioma);
        ArrayAdapter<CharSequence> idiomaAdapter = ArrayAdapter.createFromResource(this, R.array.idiomas_array, android.R.layout.simple_spinner_item);
        idiomaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        idioma.setAdapter(idiomaAdapter);


        //File imagesFolder = new File(
                //Environment.getExternalStorageDirectory(), "AndroidFacil");
        //imagesFolder.mkdirs();

        //final File image = new File(imagesFolder, "foto.jpg");

        addFoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                }

                //startActivity(intent);
           /*    Uri output = Uri.fromFile(image);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                startActivityForResult(intent,1);

                */
                return false;
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference mDataBase = FirebaseDatabase.getInstance().getReference().child("books");
                DatabaseReference currentUserDB = FirebaseDatabase.getInstance().getReference().child("users").child(mFirebaseAuth.getCurrentUser().getUid());
                DatabaseReference currentBookDB = mDataBase.child(titulo.getText().toString()+ "@" + mFirebaseAuth.getCurrentUser().getUid());

                //id_book = title + @ + uid_user

                currentBookDB.child("title").setValue(titulo.getText().toString());
                currentBookDB.child("author").setValue(autor.getText().toString());
                currentBookDB.child("description").setValue(descripcion.getText().toString());
                currentBookDB.child("language").setValue(idioma.getSelectedItem().toString());
                currentBookDB.child("owner").setValue(mFirebaseAuth.getCurrentUser().getUid());
                currentBookDB.child("image").setValue("default");

                if(opcionAventura.isChecked()) currentBookDB.child("genre").child("aventura").setValue("true");
                if(opcionCientificos.isChecked()) currentBookDB.child("genre").child("cientificos").setValue("true");
                if(opcionComic.isChecked()) currentBookDB.child("genre").child("comic").setValue("true");
                if(opcionDrama.isChecked()) currentBookDB.child("genre").child("drama").setValue("true");
                if(opcionInstructivos.isChecked()) currentBookDB.child("genre").child("instructivos").setValue("true");
                if(opcionNovela.isChecked()) currentBookDB.child("genre").child("novela").setValue("true");
                if(opcionPoesia.isChecked()) currentBookDB.child("genre").child("poesia").setValue("true");
                if(opcionReferencia.isChecked()) currentBookDB.child("genre").child("referencia").setValue("true");
                if(opcionRomance.isChecked()) currentBookDB.child("genre").child("romance").setValue("true");
                if(opcionSciFi.isChecked()) currentBookDB.child("genre").child("scifi").setValue("true");
                if(opcionTerror.isChecked()) currentBookDB.child("genre").child("terror").setValue("true");

                Toast.makeText(getApplicationContext(),"Libro Guardado!", Toast.LENGTH_SHORT).show();
                finish();

                /*
                try {
                    currentBookDB.child("imagepath").setValue("https://btrade-504da.firebaseio.com/" + imagepaths.get(imagepaths.size() - 1));
                }catch (Exception e){

                    Log.i("imagen null","error");

                }*/
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            img.setImageBitmap(imageBitmap);
            //DatabaseReference referencia=FirebaseDatabase.getInstance().getReferenceFromUrl("gs://btrade-504da.appspot.com");
            FirebaseStorage fbstorage=FirebaseStorage.getInstance();
            imagepaths.add("fotolibro"+imagepaths.size());
            StorageReference storageReference=fbstorage.getReferenceFromUrl("gs://btrade-504da.appspot.com").child(imagepaths.get(imagepaths.size()-1).toString()+".png");
            ImageView mImageView=img;
            mImageView.setDrawingCacheEnabled(true);
            mImageView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            mImageView.layout(0, 0, mImageView.getMeasuredWidth(), mImageView.getMeasuredHeight());
            mImageView.buildDrawingCache();
            Bitmap bitmap = Bitmap.createBitmap(mImageView.getDrawingCache());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] datas = outputStream.toByteArray();
            cont++;


            UploadTask uploadTask = storageReference.putBytes(datas);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                }
            });


        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: //hago un case por si en un futuro agrego mas opciones
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
