package com.example.noe.btrade2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewProfile extends AppCompatActivity {

    public static final String TAG = "Profile Activity";
    ImageView imageProfile;
    ImageView imageBackground;

    private TextView mTextViewName;
    private TextView mTextViewPhone;
    private TextView mTextViewEmail;
    private TextView mTextViewGender;

    ArrayList<GBItem> items = new ArrayList<GBItem>();

    private FirebaseAuth mAuth;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final String id = getIntent().getStringExtra("ID");

        imageProfile = (ImageView)findViewById(R.id.c_profile);
        imageBackground = (ImageView)findViewById(R.id.c_bg);

        mTextViewName = (TextView) findViewById(R.id.name);
        mTextViewPhone = (TextView) findViewById(R.id.phonenumber);
        mTextViewEmail = (TextView) findViewById(R.id.emailaddress);
        mTextViewGender = (TextView) findViewById(R.id.gender);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_gen);
        mRecyclerView.setHasFixedSize(true);

        mTextViewName.setText("");mTextViewPhone.setText("");
        mTextViewEmail.setText("");mTextViewGender.setText("");

        mAuth = FirebaseAuth.getInstance();

        DatabaseReference userDB = FirebaseDatabase.getInstance().getReference().child("users");
        userDB.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mTextViewName.setText(dataSnapshot.child("name").getValue().toString());
                getSupportActionBar().setTitle(dataSnapshot.child("name").getValue().toString());
                mTextViewPhone.setText(dataSnapshot.child("phone").getValue().toString());
                mTextViewEmail.setText(dataSnapshot.child("email").getValue().toString());
                mTextViewGender.setText(dataSnapshot.child("gender").getValue().toString());

                String imageUrl1 = dataSnapshot.child("img_profile").getValue().toString();
                if (!imageUrl1.equals("default") || TextUtils.isEmpty(imageUrl1))
                    Picasso.with(ViewProfile.this).load(imageUrl1).into(imageProfile);
                Picasso.with(ViewProfile.this).load(imageUrl1).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(imageProfile);

                String imageUrl2 = dataSnapshot.child("img_bg").getValue().toString();
                if (!imageUrl2.equals("default") || TextUtils.isEmpty(imageUrl2))
                    Picasso.with(ViewProfile.this).load(imageUrl2).into(imageBackground);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        mDatabase.child(id).child("favorite").addListenerForSingleValueEvent(new ValueEventListener() {
            //Adapter Intereses
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    //Log.d(TAG, ds.getKey().toString());
                    if(ds.getValue().toString().equals("true")){
                        int resId = getResources().getIdentifier(ds.getKey().toString(), "drawable", getPackageName());
                        String gb_name = getResources().getString(getApplicationContext().getResources().getIdentifier(ds.getKey().toString(), "string", getPackageName()));

                        GBItem item = new GBItem(gb_name, resId);
                        items.add(item);
                    }
                }
                //Log.d(TAG, ""+items.size());

                mAdapter = new GBAdapter(ViewProfile.this, items);
                mRecyclerView.setAdapter(mAdapter);
                mLayoutManager = new LinearLayoutManager(ViewProfile.this);
                mRecyclerView.setLayoutManager(mLayoutManager);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                //...
            }

        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
