package com.example.noe.btrade2;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String mId;
    private String mName;
    private String mEmail;
    private String mPhone;
    private String mGender;
    private String mImgProfile;
    private String mImgBg;

    public User(){

    }

    public User(String id, String name, String email, String phone, String gender, String img, String bg) {
        setId(id);setEmail(email);
        setName(name);setPhone(phone);
        setGender(gender);setImgProfile(img);
        setImgBg(bg);
    }

    public String getId() {return mId;}
    public void setId(String id) {this.mId = id;}

    public String getEmail() {return mEmail;}
    public void setEmail(String email) {this.mEmail = email;}

    public String getName() {return mName;}
    public void setName(String name) {this.mName = name;}

    public String getPhone() {return mPhone;}
    public void setPhone(String phone) {this.mPhone = phone;}

    public String getGender() {return mGender;}
    public void setGender(String gender) {this.mGender = gender;}

    public String getImgProfile() {return mImgProfile;}
    public void setImgProfile(String img) {this.mImgProfile = img;}

    public String getImgBg() {return mImgBg;}
    public void setImgBg(String bg) {this.mImgBg = bg;}


    protected User(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mEmail = in.readString();
        mPhone = in.readString();
        mGender = in.readString();
        mImgProfile = in.readString();
        mImgBg = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mEmail);
        dest.writeString(mPhone);
        dest.writeString(mGender);
        dest.writeString(mImgProfile);
        dest.writeString(mImgBg);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
