package com.example.noe.btrade2;

public class Book {
    private String mTitle, mAuthor, mDescription, mOwner, mImageSource, mId;

    public Book(String title, String author, String description, String owner, String imageSource) {
        setTitle(title);
        setAuthor(author);
        setDescription(description);
        setOwner(owner);
        setImageSource(imageSource);
    }

    public String getTitle() {return mTitle;}
    public void setTitle(String title) {mTitle = title;}

    public String getAuthor() {return mAuthor;}
    public void setAuthor(String author) {mAuthor = author;}

    public String getDescription() {return mDescription;}
    public void setDescription(String description) {mDescription = description;}

    public String getOwner() {return mOwner;}
    public void setOwner(String owner) {mOwner = owner;}

    public String getImageSource() {return mImageSource;}
    public void setImageSource(String imageSource) {mImageSource = imageSource;}

    public String getId(){return mId;}
    public void setId(String id){mId= id;}

    public String toString() {
        return getTitle()+" "+getAuthor()+" "+getDescription()+" "+getOwner()+" "+getImageSource();
    }
}
