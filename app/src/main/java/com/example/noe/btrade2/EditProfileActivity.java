package com.example.noe.btrade2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class EditProfileActivity extends AppCompatActivity {

    ImageView imageProfile;
    ImageView imageBackground;

    private EditText name;
    private EditText phone;
    private TextView email;

    private RadioButton radioM;
    private RadioButton radioF;

    private ImageButton editFotoP;
    private ImageButton editFotoBG;

    private CheckBox opcionAventura;
    private CheckBox opcionCientificos;
    private CheckBox opcionComic;
    private CheckBox opcionDrama;
    private CheckBox opcionInstructivos;
    private CheckBox opcionNovela;
    private CheckBox opcionPoesia;
    private CheckBox opcionReferencia;
    private CheckBox opcionRomance;
    private CheckBox opcionSciFi;
    private CheckBox opcionTerror;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    private User user;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageProfile = (ImageView)findViewById(R.id.edit_profile);
        imageBackground = (ImageView)findViewById(R.id.edit_bg);

        name = (EditText) findViewById(R.id.edit_name);
        phone = (EditText) findViewById(R.id.edit_phonenumber);
        email = (TextView) findViewById(R.id.edit_emailaddress);
        radioM = (RadioButton) findViewById(R.id.edit_radio_male);
        radioF = (RadioButton) findViewById(R.id.edit_radio_female);

        editFotoP = (ImageButton) findViewById(R.id.edit_img_p);
        editFotoBG = (ImageButton) findViewById(R.id.edit_img_bg);

        //Checkbox
        opcionAventura      = (CheckBox) findViewById(R.id.edit_opcion_aventura);
        opcionCientificos   = (CheckBox) findViewById(R.id.edit_opcion_cientificos);
        opcionComic         = (CheckBox) findViewById(R.id.edit_opcion_comic);
        opcionDrama         = (CheckBox) findViewById(R.id.edit_opcion_drama);
        opcionInstructivos  = (CheckBox) findViewById(R.id.edit_opcion_instructivos);
        opcionNovela        = (CheckBox) findViewById(R.id.edit_opcion_novela);
        opcionPoesia        = (CheckBox) findViewById(R.id.edit_opcion_poesia);
        opcionReferencia    = (CheckBox) findViewById(R.id.edit_opcion_referencia);
        opcionRomance       = (CheckBox) findViewById(R.id.edit_opcion_romance);
        opcionSciFi         = (CheckBox) findViewById(R.id.edit_opcion_scifi);
        opcionTerror        = (CheckBox) findViewById(R.id.edit_opcion_terror);

        mAuth = FirebaseAuth.getInstance();

        user = getIntent().getExtras().getParcelable("usuario");

        name.setText(user.getName());
        phone.setText(user.getPhone());
        email.setText(user.getEmail());
        if(user.getGender().equals("Masculino")) radioM.setChecked(true);
        else if(user.getGender().equals("Femenino")) radioF.setChecked(true);

        String imageUrl1 = user.getImgProfile();
        if (!imageUrl1.equals("default") || TextUtils.isEmpty(imageUrl1))
            Picasso.with(EditProfileActivity.this).load(imageUrl1).into(imageProfile);

        String imageUrl2 = user.getImgBg();
        if (!imageUrl2.equals("default") || TextUtils.isEmpty(imageUrl2))
            Picasso.with(EditProfileActivity.this).load(imageUrl2).into(imageBackground);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    mStorage = FirebaseStorage.getInstance().getReference();
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users");

                    mDatabase.child(firebaseAuth.getCurrentUser().getUid()).child("favorite").addListenerForSingleValueEvent(new ValueEventListener() {
                        //Adapter Intereses
                        @Override
                        public void onDataChange(DataSnapshot ds) {

                                if(ds.child("aventura").exists()) opcionAventura.setChecked(true);
                                if(ds.child("cientificos").exists()) opcionCientificos.setChecked(true);
                                if(ds.child("comic").exists()) opcionComic.setChecked(true);
                                if(ds.child("drama").exists()) opcionDrama.setChecked(true);
                                if(ds.child("instructivos").exists()) opcionInstructivos.setChecked(true);
                                if(ds.child("novela").exists()) opcionNovela.setChecked(true);
                                if(ds.child("poesia").exists()) opcionPoesia.setChecked(true);
                                if(ds.child("referencia").exists()) opcionReferencia.setChecked(true);
                                if(ds.child("romance").exists()) opcionRomance.setChecked(true);
                                if(ds.child("scifi").exists()) opcionSciFi.setChecked(true);
                                if(ds.child("terror").exists()) opcionTerror.setChecked(true);

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            //...
                        }

                    });

                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }

            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            case R.id.p_check:
                //metodoEdit()

                final DatabaseReference currentUserDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getCurrentUser().getUid());
                currentUserDB.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(opcionAventura.isChecked()) currentUserDB.child("favorite").child("aventura").setValue("true");
                        else if(dataSnapshot.child("favorite").child("aventura").exists()) currentUserDB.child("favorite").child("aventura").removeValue();
                        if(opcionCientificos.isChecked()) currentUserDB.child("favorite").child("cientificos").setValue("true");
                        else if(dataSnapshot.child("favorite").child("cientificos").exists()) currentUserDB.child("favorite").child("cientificos").removeValue();
                        if(opcionComic.isChecked()) currentUserDB.child("favorite").child("comic").setValue("true");
                        else if(dataSnapshot.child("favorite").child("comic").exists()) currentUserDB.child("favorite").child("comic").removeValue();
                        if(opcionDrama.isChecked()) currentUserDB.child("favorite").child("drama").setValue("true");
                        else if(dataSnapshot.child("favorite").child("drama").exists()) currentUserDB.child("favorite").child("drama").removeValue();
                        if(opcionInstructivos.isChecked()) currentUserDB.child("favorite").child("instructivos").setValue("true");
                        else if(dataSnapshot.child("favorite").child("instructivos").exists()) currentUserDB.child("favorite").child("instructivos").removeValue();
                        if(opcionNovela.isChecked()) currentUserDB.child("favorite").child("novela").setValue("true");
                        else if(dataSnapshot.child("favorite").child("novela").exists()) currentUserDB.child("favorite").child("novela").removeValue();
                        if(opcionPoesia.isChecked()) currentUserDB.child("favorite").child("poesia").setValue("true");
                        else if(dataSnapshot.child("favorite").child("poesia").exists()) currentUserDB.child("favorite").child("poesia").removeValue();
                        if(opcionReferencia.isChecked()) currentUserDB.child("favorite").child("referencia").setValue("true");
                        else if(dataSnapshot.child("favorite").child("referencia").exists()) currentUserDB.child("favorite").child("referencia").removeValue();
                        if(opcionRomance.isChecked()) currentUserDB.child("favorite").child("romance").setValue("true");
                        else if(dataSnapshot.child("favorite").child("romance").exists()) currentUserDB.child("favorite").child("romance").removeValue();
                        if(opcionSciFi.isChecked()) currentUserDB.child("favorite").child("scifi").setValue("true");
                        else if(dataSnapshot.child("favorite").child("scifi").exists()) currentUserDB.child("favorite").child("scifi").removeValue();
                        if(opcionTerror.isChecked()) currentUserDB.child("favorite").child("terror").setValue("true");
                        else if(dataSnapshot.child("favorite").child("terror").exists()) currentUserDB.child("favorite").child("terror").removeValue();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                if(radioM.isChecked()) {currentUserDB.child("gender").setValue("Masculino"); user.setGender("Masculino");}
                if(radioF.isChecked()) {currentUserDB.child("gender").setValue("Femenino"); user.setGender("Femenino");}

                currentUserDB.child("name").setValue(name.getText().toString().trim());
                user.setName(name.getText().toString().trim());

                currentUserDB.child("phone").setValue(phone.getText().toString().trim());
                user.setPhone(phone.getText().toString().trim());

                Intent intent = new Intent(this, ProfileActivity.class);
                intent.putExtra("usuario", user);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
