package com.example.noe.btrade2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;

public class ViewBook extends AppCompatActivity {

    private ImageView mImageView;
    private TextView mTextViewTitle;
    private TextView mTextViewAuthor;
    private TextView mTextViewDescription;
    private TextView mTextViewOwner;

    private DatabaseReference mDatabase;
    private final String TAG = "View Book";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<GBItem> mGenres;

    private FirebaseAuth mAuth;

    private Button mVerPerfil;
    private Button mSolicitar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_book);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String id = getIntent().getStringExtra("ID");
        final String split[];
        split = id.split("@");

        getSupportActionBar().setTitle(split[0]);

        final String IdOwner = split[1];

        mGenres = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.vbook_genres);
        mRecyclerView.setHasFixedSize(true);

        mImageView = (ImageView) findViewById(R.id.vbook_image);
        mTextViewTitle = (TextView) findViewById(R.id.vbook_title);
        mTextViewAuthor = (TextView) findViewById(R.id.vbook_author);
        mTextViewDescription = (TextView) findViewById(R.id.vbook_description);
        mTextViewOwner = (TextView) findViewById(R.id.vbook_owner);
        mVerPerfil = (Button) findViewById(R.id.button_view_profile);
        mSolicitar = (Button) findViewById(R.id.button_solicitar);

        mAuth = FirebaseAuth.getInstance();

        DatabaseReference mDBOwner = FirebaseDatabase.getInstance().getReference().child("users").child(IdOwner);
        mDBOwner.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mTextViewOwner.setText("Propietario: "+dataSnapshot.child("name").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference().child("books").child(id);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String title, author, description, owner, imageSource, id;
                title = dataSnapshot.child("title").getValue().toString();
                author = dataSnapshot.child("author").getValue().toString();
                description = dataSnapshot.child("description").getValue().toString();
                imageSource = dataSnapshot.child("image").getValue().toString();
                mTextViewTitle.setText(title);
                mTextViewAuthor.setText(author);
                mTextViewDescription.setText(description);
                Picasso.with(getApplicationContext()).load(imageSource).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(mImageView);

                for(DataSnapshot ds: dataSnapshot.child("genre").getChildren()){
                    int resId = getResources().getIdentifier(ds.getKey().toString(), "drawable", getPackageName());
                    String gb_name = getResources().getString(getApplicationContext().getResources().getIdentifier(ds.getKey().toString(), "string", getPackageName()));

                    GBItem item = new GBItem(gb_name, resId);
                    mGenres.add(item);
                }

                mAdapter = new GBAdapter(ViewBook.this, mGenres);
                mRecyclerView.setAdapter(mAdapter);
                mLayoutManager = new LinearLayoutManager(ViewBook.this);
                mRecyclerView.setLayoutManager(mLayoutManager);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mVerPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ViewProfile.class);
                intent.putExtra("ID", IdOwner);
                startActivity(intent);

            }
        });

        mSolicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatabaseReference DB = FirebaseDatabase.getInstance().getReference().child("solicitud").child(getRandomString()+"@"+mAuth.getCurrentUser().getUid()+"@"+IdOwner);
                DB.child("estado").setValue("pendiente");
                DB.child("libro").setValue(split[0]);

                final String[] userName = new String[1];
                final String[] userImg = new String[1];

                DatabaseReference userOwner = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getCurrentUser().getUid());
                userOwner.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        userName[0] = dataSnapshot.child("name").getValue().toString();
                        userImg[0] = dataSnapshot.child("img_profile").getValue().toString();

                        DB.child("solicitante").setValue(userName[0] +"@"+ userImg[0]);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                Toast.makeText(getApplicationContext(),"Solicitud Enviada!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    public String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(50, random).toString(20);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
