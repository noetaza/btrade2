package com.example.noe.btrade2;

import android.provider.BaseColumns;


public class LibrosEspera {

    public LibrosEspera(){}


    public static abstract class FeedEspera implements BaseColumns {
        public static final String BOOK_ID = "id";
        public static final String BOOK_NAME = "name";
        public static final String BOOK_AUTHOR = "author";
        public static final String BOOK_DESCRIPTION = "description";
        public static final String BOOK_OWNER = "owner";
        public static final String BOOK_IMAGE = "image";
        public static final String DATABASE_NAME = "book_database";
        public static final String TABLE_NAME = "book";

    }
}
