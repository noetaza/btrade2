package com.example.noe.btrade2;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.math.BigInteger;
import java.security.SecureRandom;

public class PostRegisterActivity extends AppCompatActivity {

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;

    String mPath;
    String mPathI = "default";
    String mPathBG = "default";

    private ImageView img;
    private ImageView bg;
    private Button confirmar;
    private Button omitir;
    private ImageButton addFotoP;
    private ImageButton addFotoBG;

    private ConstraintLayout mC;
    private ProgressDialog progressDialog;

    private CheckBox opcionAventura;
    private CheckBox opcionCientificos;
    private CheckBox opcionComic;
    private CheckBox opcionDrama;
    private CheckBox opcionInstructivos;
    private CheckBox opcionNovela;
    private CheckBox opcionPoesia;
    private CheckBox opcionReferencia;
    private CheckBox opcionRomance;
    private CheckBox opcionSciFi;
    private CheckBox opcionTerror;

    private FirebaseAuth mFirebaseAuth;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    private int code_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_register);

        img = (ImageView) findViewById(R.id.c_profile);
        bg = (ImageView) findViewById(R.id.c_bg);
        confirmar = (Button)findViewById(R.id.listo);
        omitir = (Button)findViewById(R.id.omitir);
        addFotoP = (ImageButton) findViewById(R.id.add_img_p);
        addFotoBG = (ImageButton) findViewById(R.id.add_img_bg);
        mC = (ConstraintLayout) findViewById(R.id.pr_c);

        //Checkbox
        opcionAventura      = (CheckBox) findViewById(R.id.opcion_aventura);
        opcionCientificos   = (CheckBox) findViewById(R.id.opcion_cientificos);
        opcionComic         = (CheckBox) findViewById(R.id.opcion_comic);
        opcionDrama         = (CheckBox) findViewById(R.id.opcion_drama);
        opcionInstructivos  = (CheckBox) findViewById(R.id.opcion_instructivos);
        opcionNovela        = (CheckBox) findViewById(R.id.opcion_novela);
        opcionPoesia        = (CheckBox) findViewById(R.id.opcion_poesia);
        opcionReferencia    = (CheckBox) findViewById(R.id.opcion_referencia);
        opcionRomance       = (CheckBox) findViewById(R.id.opcion_romance);
        opcionSciFi         = (CheckBox) findViewById(R.id.opcion_scifi);
        opcionTerror        = (CheckBox) findViewById(R.id.opcion_terror);

        progressDialog = new ProgressDialog(this);

        mFirebaseAuth = FirebaseAuth.getInstance();

        if(mayRequestStoragePermission()){
            addFotoBG.setEnabled(true);
            addFotoP.setEnabled(true);
        } else{
            addFotoBG.setEnabled(false);
            addFotoP.setEnabled(false);
        }

        addFotoP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code_img = 0;
                showOptions();
            }
        });

        addFotoBG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code_img = 1;
                showOptions();
            }
        });

        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference currentUserDB = FirebaseDatabase.getInstance().getReference().child("users").child(mFirebaseAuth.getCurrentUser().getUid());

                if(opcionAventura.isChecked()) currentUserDB.child("favorite").child("aventura").setValue("true");
                if(opcionCientificos.isChecked()) currentUserDB.child("favorite").child("cientificos").setValue("true");
                if(opcionComic.isChecked()) currentUserDB.child("favorite").child("comic").setValue("true");
                if(opcionDrama.isChecked()) currentUserDB.child("favorite").child("drama").setValue("true");
                if(opcionInstructivos.isChecked()) currentUserDB.child("favorite").child("instructivos").setValue("true");
                if(opcionNovela.isChecked()) currentUserDB.child("favorite").child("novela").setValue("true");
                if(opcionPoesia.isChecked()) currentUserDB.child("favorite").child("poesia").setValue("true");
                if(opcionReferencia.isChecked()) currentUserDB.child("favorite").child("referencia").setValue("true");
                if(opcionRomance.isChecked()) currentUserDB.child("favorite").child("romance").setValue("true");
                if(opcionSciFi.isChecked()) currentUserDB.child("favorite").child("scifi").setValue("true");
                if(opcionTerror.isChecked()) currentUserDB.child("favorite").child("terror").setValue("true");

                /*
                try {
                    currentBookDB.child("imagepath").setValue("https://btrade-504da.firebaseio.com/" + imagepaths.get(imagepaths.size() - 1));
                }catch (Exception e){

                    Log.i("imagen null","error");

                }*/
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
                finish();
            }
        });

        omitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
                finish();

            }
        });

    }

    private boolean mayRequestStoragePermission() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))
            return true;

        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) || (shouldShowRequestPermissionRationale(CAMERA))){
            Snackbar.make(mC, "Los permisos son necesarios para poder usar la aplicación",
                    Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
                }
            });
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        }

        return false;
    }

    private void showOptions() {
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(PostRegisterActivity.this);
        builder.setTitle("Elige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    openCamera();
                }else if(option[which] == "Elegir de galeria"){
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", mPath);
        outState.putInt("_img", code_img);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mPath = savedInstanceState.getString("file_path");
        code_img = savedInstanceState.getInt("_img");
    }

    public String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(50, random).toString(32);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                }
                            });

                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    if(code_img == 0) {
                        img.setImageBitmap(bitmap);
                    }
                    if(code_img == 1) {
                        bg.setImageBitmap(bitmap);
                    }
                    break;

                case SELECT_PICTURE:
                    Uri path = data.getData();
                    if(code_img == 0) img.setImageURI(path);
                    if(code_img == 1) bg.setImageURI(path);
                    break;

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(PostRegisterActivity.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                addFotoP.setEnabled(true);
                addFotoBG.setEnabled(true);
            }
        }else{
            showExplanation();
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PostRegisterActivity.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }
}
