package com.example.noe.btrade2;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GBAdapter extends RecyclerView.Adapter<GBAdapter.ViewHolder> {

    Context context;
    ArrayList<GBItem> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imgGen);
            mTextView = (TextView) itemView.findViewById(R.id.nameGen);
        }
    }

    public GBAdapter(Context c, ArrayList<GBItem> it) {
        context = c;
        items = it;
    }

    @Override
    public GBAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.content_gb, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GBItem user = items.get(position);
        holder.mImageView.setImageResource(user.getPictureResId());
        holder.mTextView.setText(user.getName());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

}
