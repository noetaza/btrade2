package com.example.noe.btrade2;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private DatabaseReference mBookDatabase;
    private String mCurrentUserId;
    private DrawerLayout draweL;
    private TabLayout tab;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private User user;
    private FirebaseAuth mAuth;

    /*PARA BUSQUEDA*/
    private ArrayList mBooks;
    private String[] mArrayData={"Sin coincidencias"};
    /*PARA BUSQUEDA*/

    private static final String TAG = "Home Activity";

    ImageView img_toolbar ;
    TextView name_toolbar;
    TextView correo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navegar_toolbar);

        mAuth = FirebaseAuth.getInstance();

        mCurrentUserId = mAuth.getCurrentUser().getUid();
        user = new User();

        Log.d(TAG,  mCurrentUserId);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mCurrentUserId);

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                user.setId(mCurrentUserId);
                user.setName(dataSnapshot.child("name").getValue().toString());
                user.setEmail(dataSnapshot.child("email").getValue().toString());
                user.setPhone(dataSnapshot.child("phone").getValue().toString());
                user.setGender(dataSnapshot.child("gender").getValue().toString());
                user.setImgProfile(dataSnapshot.child("img_profile").getValue().toString());
                user.setImgBg(dataSnapshot.child("img_bg").getValue().toString());

           }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        mBookDatabase = FirebaseDatabase.getInstance().getReference().child("books");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        draweL= (DrawerLayout)findViewById(R.id.navegar);
        NavigationView navegtion=(NavigationView)findViewById(R.id.navegation);

            View header= navegtion.inflateHeaderView(R.layout.header_navega);
            name_toolbar =(TextView)header.findViewById(R.id.usuario_Header);
            img_toolbar=(ImageView)header.findViewById(R.id.imagen_header);
            correo=(TextView)header.findViewById(R.id.correo_header);


        navegtion.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if(item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                draweL.closeDrawers();
                switch (item.getItemId()){
                    case R.id.perfil:
                        Intent intent= new Intent(getApplicationContext(),ProfileActivity.class);
                        intent.putExtra("usuario", user);
                        startActivity(intent);
                        //Toast.makeText(getApplicationContext(),"All Mail Selected", Toast.LENGTH_SHORT).show();

                        return true;
                    case R.id.mis_libros:
                        Intent intent2 = new Intent(getApplicationContext(), MyBooksActivity.class);
                        startActivity(intent2);

                        return true;

                    case R.id.log_out:
                        FirebaseAuth.getInstance().signOut();
                        finish();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        return true;
                }
                return false;
            }
        });

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);//This description is commonly used for accessibility/screen readers when the Home action is enabled
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//o set several display options at once, see the setDisplayOptions methods
        getSupportActionBar().setHomeButtonEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container1);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        tab=(TabLayout)findViewById(R.id.tabs_H);
        tab.setupWithViewPager(mViewPager);


        TabLayout.Tab tabCall = tab.getTabAt(0);
        tabCall.setIcon(R.drawable.pestanhas_tabs3);

        TabLayout.Tab tabCall2 = tab.getTabAt(1);
        tabCall2.setIcon(R.drawable.pestanha_tabs);

        TabLayout.Tab tabCall3= tab.getTabAt(2);
        tabCall3.setIcon(R.drawable.pestanhas_tabs2);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Title");

        //final android.app.ActionBar actionBar= getActionBar();
        //actionBar.setNavigationMode();
        //tab= (TabLayout)findViewById(R.id.tab_home);
        //tab.setupWithViewPager(mViewPager);

    }

    private SearchView mSearchView;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.itemtoolbar, menu);
        MenuItem searchItem = menu.findItem(R.id.buscar);
        SearchManager searchManager = (SearchManager)  Home.this.getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        final String[] from = new String[] {"name"};
        final int[] to = new int[] {android.R.id.text1};
        final SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                Home.this, android.R.layout.simple_spinner_dropdown_item,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);;
        if(searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (searchItem != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(Home.this.getComponentName()));
            mSearchView.setIconified(false);
            mSearchView.setSuggestionsAdapter(simpleCursorAdapter);
            mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionSelect(int position) {
                    return true;
                }

                @Override
                public boolean onSuggestionClick(int position) {
                    CursorAdapter cursorAdapter = mSearchView.getSuggestionsAdapter();
                    Cursor cursor = cursorAdapter.getCursor();
                    cursor.moveToPosition(position);
                    mSearchView.setQuery(cursor.getString(1), false);
                    return true;
                }
            });

            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextChange (String newText){
                    final MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
                    final String text = ""+newText;
                    mBooks = new ArrayList<Book>();

                    mBookDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String title, author, description, owner, imageSource, id;
                            Book book;
                            String bookString;
                            int k =0;
                            for (DataSnapshot ds: dataSnapshot.getChildren()) {
                                title = ds.child("title").getValue().toString();
                                author = ds.child("author").getValue().toString();
                                description = ds.child("description").getValue().toString();
                                owner = ds.child("owner").getValue().toString();
                                imageSource = ds.child("image").getValue().toString();

                                if(title.toLowerCase().contains(text.toLowerCase()) || author.toLowerCase().contains(text.toLowerCase())) {
                                    book = new Book(title, author, description, owner, imageSource);
                                    book.setId(title+"@"+owner);
                                    mBooks.add(book);
                                }
                            }
                            Book b ;
                            mArrayData = new String[mBooks.size()];
                            for(int i= 0; i < mBooks.size(); i ++) {
                                b = (Book) mBooks.get(i);
                                mArrayData[i] = b.getTitle()+"-"+b.getAuthor();
                                Log.d(TAG, mArrayData[i]+"@"+text);
                                matrixCursor.addRow(new Object[]{i, mArrayData[i]});
                            }
                            simpleCursorAdapter.changeCursor(matrixCursor);

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                    return false;
                }


                @Override
                public boolean onQueryTextSubmit(String query) {
                    Intent intent = SearchBookActivity.newIntent(getApplicationContext(), query);
                    startActivity(intent);
                    return false;
                }
            });

        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                name_toolbar.setText(user.getName());
                correo.setText(user.getEmail());
                Picasso.with(Home.this).load(user.getImgProfile()).error(R.drawable.ic_error).placeholder(R.drawable.progress_animation).into(img_toolbar);

                draweL.openDrawer(GravityCompat.START);

        Log.i("" + item.getClass().getName(), android.R.id.home + "");

                return true;
            case R.id.buscar:
                Log.i(""+item.getItemId(),R.id.home+"");
                return true;
            case R.id.agregar:
                Intent inten= new Intent(getApplicationContext(),NewBook.class);
                startActivity(inten);
                Log.i(""+item.getItemId(),R.id.home+"");
                return true;
            case R.id.ayuda:
                Log.i(""+item.getItemId(),R.id.home+"");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f =null;

            switch (position) {
                case 0:
                    f=Fragment_recoment.newInstance();
                    break;
                case 1:
                    f= FragmentSolicitudes.newInstance();
                    break;
                case 2:
                    f=Fragment_notifi.newInstance();
                    break;
            }
        return f;
           /// return PlaceholderFragment.newInstance(position + 1);
        }




        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
/*
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "RECOMENT";
                case 1:
                    return "NOTIFICATION";
                case 2:
                    return "ESPERA";
            }
            return null;
        }*/
    }

}
