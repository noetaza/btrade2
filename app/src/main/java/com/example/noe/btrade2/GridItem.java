package com.example.noe.btrade2;

public class GridItem {
    private String mID;
    private String mTitle;
    private String mAuthor;
    private String mOwner;
    private String mImageSource;

    public GridItem(String title, String author, String owner, String imageSource) {
        setTitle(title);
        setAuthor(author);
        setOwner(owner);
        setImageSource(imageSource);
        setId(title+"@"+owner);
    }

    public String getTitle() {return mTitle;}
    public void setTitle(String title) {mTitle = title;}

    public String getAuthor() {return mAuthor;}
    public void setAuthor(String author) {mAuthor = author;}

    public String getOwner() {return mOwner;}
    public void setOwner(String owner) {mOwner = owner;}

    public String getImageSource() {return mImageSource;}
    public void setImageSource(String imageSource) {mImageSource = imageSource;}

    public String getID(){return mID;}
    public void setId(String id){mID= id;}

}
