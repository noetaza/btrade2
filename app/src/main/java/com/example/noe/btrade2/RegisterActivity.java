package com.example.noe.btrade2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    private EditText mEmailField;
    private EditText mNameField;
    private EditText mPasswordField;
    private EditText mConfirmPasswordField;
    private EditText mPhoneField;

    private RadioButton radioM;
    private RadioButton radioF;

    private Button mRegisterButton;

    private FirebaseAuth mFirebaseAuth;
    private ProgressDialog mProgress;

    private final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    );

    private Matcher matcher;

    View focusView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mEmailField = (EditText) findViewById(R.id.register_email);
        mNameField = (EditText) findViewById(R.id.register_name);
        mPasswordField = (EditText) findViewById(R.id.register_password);
        mConfirmPasswordField = (EditText) findViewById(R.id.register_confirm_password);
        mPhoneField = (EditText) findViewById(R.id.register_phone);

        radioM = (RadioButton) findViewById(R.id.radio_male);
        radioF = (RadioButton) findViewById(R.id.radio_female);

        mRegisterButton = (Button) findViewById(R.id.register_finish_button);

        mProgress = new ProgressDialog(this);

        //Listener Register
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegister();
            }
        });

    }

    private void startRegister() {

        //Reset errors
        mEmailField.setError(null);
        mPasswordField.setError(null);
        mConfirmPasswordField.setError(null);
        mNameField.setError(null);
        mPhoneField.setError(null);

        //Valores de registro
        final String name = mNameField.getText().toString().trim();
        final String email = mEmailField.getText().toString().trim();
        final String password = mPasswordField.getText().toString().trim();
        final String confirm_password = mConfirmPasswordField.getText().toString().trim();
        final String phone = mPhoneField.getText().toString().trim();

        boolean cancel = false;

        //Verificar todos los campos no esten vacios
        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)
                && !TextUtils.isEmpty(confirm_password) && !TextUtils.isEmpty(phone)){

            //Validar Password
            if(!password.equals(confirm_password)){
                mConfirmPasswordField.setError(getString(R.string.error_confirm_password));
                focusView = mConfirmPasswordField;
                cancel = true;
            }

            if (!isPasswordValid(password)) {
                mPasswordField.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordField;
                cancel = true;
            }

            //Validar email
            if (!isEmailValid(email)) {
                mEmailField.setError(getString(R.string.error_invalid_email));
                focusView = mEmailField;
                cancel = true;
            }

            if(cancel){
                focusView.requestFocus();

            }else{
                mProgress.setMessage("Registrando...");
                mProgress.show();

                mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                mProgress.dismiss();
                                if(task.isSuccessful()){
                                    //Guarda en BD
                                    Toast.makeText(RegisterActivity.this, "Registrado", Toast.LENGTH_SHORT).show();
                                    mFirebaseAuth.signInWithEmailAndPassword(email, password);

                                    DatabaseReference mDataBase = FirebaseDatabase.getInstance().getReference().child("users");
                                    DatabaseReference currentUserDB = mDataBase.child(mFirebaseAuth.getCurrentUser().getUid());

                                    currentUserDB.child("name").setValue(name);
                                    currentUserDB.child("email").setValue(email);
                                    currentUserDB.child("phone").setValue(phone);

                                    if(radioM.isChecked()){
                                        currentUserDB.child("gender").setValue("Masculino");
                                    }else {
                                        if(radioF.isChecked()) currentUserDB.child("gender").setValue("Femenino");
                                    }

                                    currentUserDB.child("img_profile").setValue("default");
                                    currentUserDB.child("img_bg").setValue("default");

                                    //Iniciar Pantalla Principal
                                    //...
                                    Intent intent = new Intent(getApplicationContext(), PostRegisterActivity.class);
                                    startActivity(intent);
                                    finish();


                                }else{
                                    try{
                                        throw task.getException();
                                    }catch (FirebaseAuthUserCollisionException e){
                                        //Email ya registrado
                                        mEmailField.setError(getString(R.string.error_user_exists));
                                        focusView =  mEmailField;
                                        focusView.requestFocus();

                                    } catch (Exception e) {
                                        //e.printStackTrace();
                                        Log.i("ASD", "ERROR REGISTRO");
                                    }
                                }
                            }
                        });

            }

        }else{
            focusView = mEmailField;
            focusView.requestFocus();
            Toast.makeText(RegisterActivity.this, "Campos Incompletos", Toast.LENGTH_SHORT).show();
        }
    }

    //Validacion
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        matcher = EMAIL_ADDRESS_PATTERN.matcher(email);
        return matcher.matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}
